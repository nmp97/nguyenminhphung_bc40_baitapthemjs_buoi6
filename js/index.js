// EXERCISE 1
var ex1Btn = document.getElementById('ex1__btn');

function isPrime(n) {
  if (n === 0 || n === 1) {
    return false;
  }
  for (var i = 2; i < n; i++) {
    if (n % i === 0) {
      return false;
    }
  }
  return true;
}

ex1Btn.addEventListener('click', function () {
  var num = document.getElementById('number').value * 1;
  var contentHTML = '';

  for (var i = 1; i <= num; i++) {
    if (isPrime(i)) {
      contentHTML += i + ' ';
    }
  }

  document.getElementById('ex1__result').innerHTML = `Các số nguyên tố: ${contentHTML}`;
});

//EXCISE 2
var ex2Btn = document.getElementById('ex2__btn');

function checkOddEven(n) {
  if (n % 2 === 0) {
    return false;
  }
  return true;
}

ex2Btn.addEventListener('click', function () {
  var odd = '';
  var even = '';
  var countEven = 0;
  var countOdd = 0;

  for (var i = 0; i <= 100; i++) {
    if (checkOddEven(i) === true) {
      odd += i + ' ';
      countOdd += 1;
    } else {
      even += i + ' ';
      countEven += 1;
    }
  }

  document.getElementById('ex2__result').innerHTML = `
  <p>Có ${countEven} số chẵn: ${even}</p>
  <p>Có ${countOdd} số lẻ: ${odd}</p>
  `;
});

//EXCISE 3
var ex3Btn = document.getElementById('ex3__btn');

function divideThree(n) {
  if (n % 3 === 0) return true;
}

ex3Btn.addEventListener('click', function () {
  var count = 0;

  for (var i = 0; i <= 1000; i++) {
    if (divideThree(i)) {
      count++;
    }
  }

  document.getElementById('ex3__result').innerHTML = `Số chia hết cho 3 nhỏ hơn 1000: ${count} số`;
});
